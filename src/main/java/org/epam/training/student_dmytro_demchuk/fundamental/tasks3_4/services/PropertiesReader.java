package org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.services;

import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.Option;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.OptionType;

import java.util.*;

public class PropertiesReader {
    private static ResourceBundle resourceBundle;

    static {
        String environment = System.getProperty("environment");
        if (environment != null) {
            resourceBundle = ResourceBundle.getBundle(environment);
        } else {
            // Handle the case when the environment property is not set
            throw new IllegalArgumentException("Environment property is not set");
        }
    }

    public static String getSingleProperty(String key) {
        return resourceBundle.getString(key);
    }

    public static List<Option> getAllOptions(String key) {
        List<Option> options = new ArrayList<>();

        Set<String> keys = resourceBundle.keySet();

        for (String k : keys) {
            if (k.startsWith(key)) {
                String[] option = resourceBundle.getString(k).split("\\$ ");

                if (option.length == 3) {
                    options.add(new Option(option[0], option[1], OptionType.getByValue(option[2])));
                } else {
                    options.add(new Option(option[0], OptionType.getByValue(option[1])));
                }
            }
        }
        options.sort(Comparator.comparing(obj -> obj.getType().ordinal()));

        return options;
    }
}
