package org.epam.training.student_dmytro_demchuk.fundamental.tasks1_2.classes.pastebin;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

public class PasteBinMainPage extends BasePage {
    private static final String PASTEBIN_URL = "https://pastebin.com";
    protected WebDriver driver;

    @FindBy(id = "postform-text")
    private WebElement codeTextInput;

    @FindBy(xpath = "//*[@id='select2-postform-expiration-container']")
    private WebElement dropdownPasteExpiration;

    @FindBy(xpath = "//*[@id='select2-postform-format-container']")
    private WebElement dropdownSyntaxHighlighting;

    @FindBy(xpath = "//*[@id='postform-name']")
    private WebElement pasteNameTextInput;

    @FindBy(xpath = "//button[@class='btn -big']")
    private WebElement createButton;

    public PasteBinMainPage(WebDriver driver){
        super(driver);
        this.driver = driver;
    }

    public PasteCreatedPage createNewPaste(String code, String syntaxHighlighting,
                                           String expiration, String pasteName) {

        codeTextInput.sendKeys(code);

        selectAndClickOptionFromDropdown(dropdownSyntaxHighlighting, syntaxHighlighting);
        selectAndClickOptionFromDropdown(dropdownPasteExpiration, expiration);

        scrollToElementCenter(pasteNameTextInput).sendKeys(pasteName);

        scrollToElementCenter(createButton).click();

        return new PasteCreatedPage(driver);
    }

    public PasteBinMainPage open(){
        driver.get(PASTEBIN_URL);
        driver.manage().window().maximize();
        return this;
    }
}
