package org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud;

import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.Option;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.OptionType;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage {
    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    protected void selectOption(Option option) {
        switch (option.getType()) {

            case OptionType.DROPDOWN_ELEMENT:
                scrollToElementCenter(buildDropDownWebElement(option.getName())).click();
                scrollToElementCenter(buildDropdownOptionElement(option.getValue())).click();
                break;

            case OptionType.RADIO_BUTTON:
                scrollToElementCenter(buildSimpleButtonWebElement(option.getName(), option.getValue())).click();
                break;

            case OptionType.NUMBER_INPUT:
                WebElement numberInput = buildNumberInput(option.getName());
                scrollToElementCenter(numberInput).clear();
                numberInput.sendKeys(option.getValue());
                break;

            case OptionType.SWITCH:
                scrollToElementCenter(buildSwitchBox(option.getName())).click();
                break;

            default:
                throw new IllegalArgumentException("Unknown option type: " + option.getType());
        }
    }

    private WebElement buildDropDownWebElement(String sectionName) {
        return waitForElementToBeVisible(By.xpath(
                        "//span[text()='" + sectionName + "']/ancestor::div[@role='combobox']"));
    }

    private WebElement buildDropdownOptionElement(String option) {
        return waitForElementToBeVisible(By.xpath("//span[text()='" + option + "']/ancestor::li"));
    }

    private WebElement buildSimpleButtonWebElement(String sectionName, String option) {
        return driver.findElement(By.xpath(
                "//div[text()='" + sectionName + "']/following::label[text()='" + option +"']"));
    }

    private WebElement buildNumberInput(String sectionName) {
        return waitForElementToBeVisible(By.xpath("//div[text()='" + sectionName +
                "']/parent::div/following-sibling::div//input"));
    }

    private WebElement buildSwitchBox(String name){
        return waitForElementToBeVisible(
                By.xpath("//button[@aria-label='" + name + "']"));
    }

    protected WebElement waitForElementToBeClickable(WebElement element){
        return new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    protected WebElement waitForElementToBeVisible(By locator){
        return new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected WebElement scrollToElementCenter(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView({block: 'center'});", element);
        return element;
    }
}
