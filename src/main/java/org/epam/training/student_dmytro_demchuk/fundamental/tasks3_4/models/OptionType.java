package org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models;

public enum OptionType {
    SWITCH("switch"),
    RADIO_BUTTON("radio button"),
    NUMBER_INPUT("number input"),
    DROPDOWN_ELEMENT("dropdown element"),
    TEXT("text");

    private final String value;

    OptionType(String value) {
        this.value = value;
    }

    public static OptionType getByValue(String value) {
        for (OptionType optionType : values()) {
            if (optionType.value.equalsIgnoreCase(value)) {
                return optionType;
            }
        }
        throw new IllegalArgumentException("No such value found for OptionType: " + value);
    }
}
