package org.epam.training.student_dmytro_demchuk.fundamental.tasks1_2.classes.pastebin;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class PasteCreatedPage extends BasePage{
    protected WebDriver driver;

    @FindBy(xpath = "//div[@class='notice -success -post-view']")
    private WebElement pasteCreatedMessage;

    @FindBy(xpath = "//*[@class='info-top']/h1")
    private WebElement pasteName;

    @FindBy(xpath = "//div[@class='left']/a")
    private WebElement pasteSyntaxHighlighting;


    @FindBy(xpath = "//div[@class='highlighted-code']//ol//li")
    private List<WebElement> codeLines;



    public PasteCreatedPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public String getCreatedPasteName() {
        return pasteName.getText();
    }

    public String getCreatedPasteSyntaxHighlighting() {
        return pasteSyntaxHighlighting.getText();
    }

    public String getCreatedPasteCode() {
        StringBuilder codeLinesText = new StringBuilder();

        for (WebElement codeLine : codeLines) {
            codeLinesText.append(codeLine.getText()).append("\n");
        }

        codeLinesText.delete(codeLinesText.length() - 2, codeLinesText.length());

        return codeLinesText.toString();
    }

    public boolean isPasteCreatedMessageDisplayed() {
        try {
            return new WebDriverWait(driver, Duration.ofSeconds(10))
                    .until(ExpectedConditions.visibilityOf(pasteCreatedMessage))
                    .isDisplayed();
        }catch (TimeoutException e){
            return false;
        }
    }
}
