package org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GoogleCloudSearchResultPage extends BasePage {
    private final Logger logger = LogManager.getRootLogger();
    protected WebDriver driver;

    public GoogleCloudSearchResultPage(WebDriver driver){
        super(driver);
        this.driver = driver;
    }

    public GoogleCloudCalculatorPage proceedToLink(String linkName){
        waitForElementToBeVisible(By.xpath(("(//a[text()='" + linkName + "'])[1]"))
        ).click();

        logger.info("Proceeding to link {}", linkName);

        return new GoogleCloudCalculatorPage(driver);
    }
}
