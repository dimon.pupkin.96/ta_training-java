package org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.services;

import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.GoogleCloudProduct;

public class ProductCreator {
    public static final String TEST_DATA_SEARCH_REQUEST = "page.title.searchRequest";
    public static final String TEST_DATA_SEARCH_RESULT_TITLE = "page.title.searchResultTitle";
    public static final String TEST_DATA_SECTION_NAME = "page.title.sectionName";
    public static final String TEST_DATA_OPTIONS = "computeEngine.option";
    public static final String TEST_DATA_TOTAL_ESTIMATED_COST = "expectedCost";

    public static GoogleCloudProduct getProduct() {
        return new GoogleCloudProduct(
                PropertiesReader.getSingleProperty(TEST_DATA_SEARCH_REQUEST),
                PropertiesReader.getSingleProperty(TEST_DATA_SEARCH_RESULT_TITLE),
                PropertiesReader.getSingleProperty(TEST_DATA_SECTION_NAME),
                PropertiesReader.getAllOptions(TEST_DATA_OPTIONS),
                PropertiesReader.getSingleProperty(TEST_DATA_TOTAL_ESTIMATED_COST)
        );
    }

}
