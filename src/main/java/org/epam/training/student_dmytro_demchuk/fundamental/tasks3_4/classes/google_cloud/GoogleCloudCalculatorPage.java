package org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.Option;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class GoogleCloudCalculatorPage extends BasePage {
    private final Logger logger = LogManager.getRootLogger();
    protected WebDriver driver;

    @FindBy(xpath = "//span[text()='Add to estimate']")
    private WebElement addToEstimateButton;

    @FindBy(xpath = "//div[text()='Estimated cost']/following::label")
    private WebElement estimatedCostTextField;

    @FindBy(xpath = "//span[@class='close']")
    private WebElement closeHelpChatButton;

    @FindBy(xpath = "//button[@aria-label='Open Share Estimate dialog']")
    private WebElement shareEstimateButton;

    @FindBy(xpath = "//a[@track-name='open estimate summary']")
    private WebElement estimateSummaryLink;

    public GoogleCloudCalculatorPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public GoogleCloudCalculatorPage proceedToForm(String sectionName) {
        waitForElementToBeClickable(addToEstimateButton).click();
        waitForElementToBeVisible((By.xpath("//div[./h2[text()='" + sectionName + "']]"))).click();

        logger.info("Proceeding to form: {}", sectionName);
        return this;
    }

    public GoogleCloudCalculatorPage fillForm(List<Option> options) {
        for (Option option : options) {
            selectOption(option);
        }

        logger.info("Form filled with options: {}", options.toString());
        return this;
    }

    public GoogleCloudSummaryPage proceedToSummary() {
        closeHelpChatButton.click();
        shareEstimateButton.click();

        String originalWindow = driver.getWindowHandle();

        waitForElementToBeClickable(estimateSummaryLink).click();

        for (String windowHandle : driver.getWindowHandles()) {
            if(!originalWindow.contentEquals(windowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }

        logger.info("Proceeding to summary page");

        return new GoogleCloudSummaryPage(driver);
    }

    public String getEstimatedCost() {
        waitForCostUpdate();
        return estimatedCostTextField.getText();
    }

    private void waitForCostUpdate() {
        String oldCost = estimatedCostTextField.getText();

        waitForElementToBeVisible( By.xpath("//div[text()='Service cost updated']"));

        new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions
                        .not(ExpectedConditions.textToBePresentInElement(estimatedCostTextField, oldCost)));

    }
}
