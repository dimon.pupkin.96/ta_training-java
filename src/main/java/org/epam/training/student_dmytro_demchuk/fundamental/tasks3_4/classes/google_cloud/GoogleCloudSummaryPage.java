package org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud;

import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.Option;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.OptionType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class GoogleCloudSummaryPage extends BasePage{
    protected WebDriver driver;

    @FindBy(xpath = "//span[text()='Compute']/parent::div/following-sibling::div//div")
    private List<WebElement> summaryList;

    @FindBy(xpath = "//h6[text()='Total estimated cost']/following::h6")
    private WebElement totalEstimatedCost;

    public GoogleCloudSummaryPage(WebDriver driver){
        super(driver);
        this.driver = driver;
    }


    public List<Option> getSummaryList(){
        List<Option> options = new ArrayList<>();

        summaryList.stream().map(WebElement::getText).map(text -> text.split("\n")).forEach(parts -> {
            int optionsSize = (int) Math.ceil((double) parts.length / 3);
            for (int i = 0; i < optionsSize; i++) {
                options.add(new Option(parts[i * 3], parts[i * 3 + 1], OptionType.TEXT));
            }
        });

        return options;
    }

    public String getTotalEstimatedCost() {
        return totalEstimatedCost.getText();
    }
}
