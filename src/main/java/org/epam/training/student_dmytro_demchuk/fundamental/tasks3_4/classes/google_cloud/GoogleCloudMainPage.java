package org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleCloudMainPage extends BasePage {
    private final Logger logger = LogManager.getRootLogger();

    private static final String GOOGLE_CLOUD_URL = "https://cloud.google.com/";
    protected WebDriver driver;
    @FindBy(xpath = "//span[text()='\uE8B6']/parent::div/parent::div")
    private WebElement searchButton;

    @FindBy(xpath = "//input[@aria-label='Search']")
    private WebElement searchInput;

    @FindBy(xpath = "//i[@role='button'and@aria-label='Search']")
    private WebElement confirmSearchButton;

    public GoogleCloudMainPage(WebDriver driver){
        super(driver);
        this.driver = driver;
    }

    public GoogleCloudMainPage open(){
        driver.get(GOOGLE_CLOUD_URL);
        driver.manage().window().maximize();
        return this;
    }

    public GoogleCloudSearchResultPage search(String searchRequest){
        searchButton.click();
        searchInput.sendKeys(searchRequest);
        confirmSearchButton.click();

        logger.info("Search for {} is performed", searchRequest);

        return new GoogleCloudSearchResultPage(driver);
    }

}
