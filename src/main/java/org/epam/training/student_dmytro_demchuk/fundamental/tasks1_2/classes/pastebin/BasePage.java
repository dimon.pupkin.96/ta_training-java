package org.epam.training.student_dmytro_demchuk.fundamental.tasks1_2.classes.pastebin;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage {
    protected WebDriver driver;

    public BasePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    protected void selectAndClickOptionFromDropdown(WebElement dropdown, String option){
        scrollToElementCenter(dropdown);

        waitForElementToBeClickable(dropdown).click();
        WebElement dropdownOption = driver.findElement(By.xpath("//li[text()='" + option + "']"));
        waitForElementToBeClickable(dropdownOption).click();
    }

    protected WebElement waitForElementToBeClickable(WebElement element){
        return new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    protected WebElement scrollToElementCenter(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView({block: 'center'});", element);
        return element;
    }
}
