package org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models;

import java.util.List;

public record GoogleCloudProduct (String searchRequest,
                                  String linkName,
                                  String sectionName,
                                  List<Option> options,
                                  String totalEstimatedCost) {}
