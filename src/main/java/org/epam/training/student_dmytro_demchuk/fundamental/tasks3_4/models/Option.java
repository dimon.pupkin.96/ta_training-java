package org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models;

public class Option {
    private String name;
    private String value;
    private OptionType type;

    public Option(String optionName, String optionValue, OptionType optionType) {
        this.name = optionName;
        this.value = optionValue;
        this.type = optionType;
    }

    public Option(String optionName, OptionType optionType) {
        this.name = optionName;
        this.type = optionType;
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public OptionType getType() {
        return this.type;
    }

    @Override
    public String toString() {
        return "Option{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", type=" + type.toString() +
                '}';
    }
}
