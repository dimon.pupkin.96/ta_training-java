package task3_tests;

import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud.GoogleCloudCalculatorPage;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud.GoogleCloudMainPage;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud.GoogleCloudSummaryPage;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.Option;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.OptionType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestPricingCalculator {
    private WebDriver webDriver;


    @BeforeEach
    public void setUp(){
        webDriver = new ChromeDriver();
    }

    @ParameterizedTest
    @MethodSource("testDataForCalculatingCost")
    public void testCalculateCost(String searchQuery, String linkText, String sectionName,
                                  List<Option> options, String expectedCost){
        GoogleCloudCalculatorPage googleCloudCalculatorPage = new GoogleCloudMainPage(webDriver)
                .open()
                .search(searchQuery)
                .proceedToLink(linkText)
                .proceedToForm(sectionName)
                .fillForm(options);

        assertEquals(expectedCost, googleCloudCalculatorPage.getEstimatedCost());
        
        GoogleCloudSummaryPage summaryPage = googleCloudCalculatorPage.proceedToSummary();
        List<Option> summaryList = summaryPage.getSummaryList();

        for (Option option : options) {
            summaryList.stream()
                    .filter(o -> o.getName().equalsIgnoreCase(option.getName()))
                    .findFirst()
                    .ifPresent(o -> assertEquals(option.getValue().toLowerCase(), o.getValue().toLowerCase()));
        }

        assertEquals(expectedCost, summaryPage.getTotalEstimatedCost());
    }

    @AfterEach
    public void tearDown(){
        webDriver.quit();
    }

    static Stream<Arguments> testDataForCalculatingCost() {
        return Stream.of(
                Arguments.of("Google Cloud Platform Pricing Calculator",
                        "Google Cloud Pricing Calculator",
                        "Compute Engine",
                        List.of(new Option("Number of Instances",
                                        "4",
                                        OptionType.NUMBER_INPUT),
                                new Option("Operating System / Software",
                                        "Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)",
                                        OptionType.DROPDOWN_ELEMENT),
                                new Option("Provisioning Model",
                                        "Regular",
                                        OptionType.RADIO_BUTTON),
                                new Option("Machine Family",
                                        "General Purpose",
                                        OptionType.DROPDOWN_ELEMENT),
                                new Option("Series",
                                        "N1",
                                        OptionType.DROPDOWN_ELEMENT),
                                new Option("Machine type",
                                        "n1-standard-8",
                                        OptionType.DROPDOWN_ELEMENT),
                                new Option("Add GPUs", OptionType.SWITCH),
                                new Option("GPU Model",
                                        "NVIDIA Tesla V100",
                                        OptionType.DROPDOWN_ELEMENT),
                                new Option("Number of GPUs",
                                        "1",
                                        OptionType.DROPDOWN_ELEMENT),
                                new Option("Local SSD",
                                        "2x375 GB",
                                        OptionType.DROPDOWN_ELEMENT),
                                new Option("Region",
                                        "Netherlands (europe-west4)",
                                        OptionType.DROPDOWN_ELEMENT),
                                new Option("Committed use discount options",
                                        "1 year",
                                        OptionType.RADIO_BUTTON)
                        ),
                        "$5,628.90"
                        )
        );
    }
}
