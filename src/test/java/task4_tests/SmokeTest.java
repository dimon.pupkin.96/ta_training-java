package task4_tests;

import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud.GoogleCloudCalculatorPage;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud.GoogleCloudMainPage;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.GoogleCloudProduct;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.services.ProductCreator;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SmokeTest extends CommonConditions {
    @Test
    public void testSmoke() {
        GoogleCloudProduct googleCloudProduct = ProductCreator.getProduct();

        GoogleCloudCalculatorPage googleCloudCalculatorPage = new GoogleCloudMainPage(driver)
                .open()
                .search(googleCloudProduct.searchRequest())
                .proceedToLink(googleCloudProduct.linkName())
                .proceedToForm(googleCloudProduct.sectionName());

        String estimatedCost = googleCloudCalculatorPage.getEstimatedCost();
        int onlyNumbers = Integer.parseInt(estimatedCost.replaceAll("\\D", ""));

        assertThat(onlyNumbers, is(greaterThanOrEqualTo(0)));
    }
}
