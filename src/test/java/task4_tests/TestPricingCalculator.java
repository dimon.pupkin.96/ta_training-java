package task4_tests;

import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud.GoogleCloudCalculatorPage;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud.GoogleCloudMainPage;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.classes.google_cloud.GoogleCloudSummaryPage;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.GoogleCloudProduct;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.models.Option;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.services.ProductCreator;
import org.testng.annotations.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class TestPricingCalculator extends CommonConditions {

    @Test
    public void testCalculateCost(){
        GoogleCloudProduct googleCloudProduct = ProductCreator.getProduct();

        GoogleCloudCalculatorPage googleCloudCalculatorPage = new GoogleCloudMainPage(driver)
                .open()
                .search(googleCloudProduct.searchRequest())
                .proceedToLink(googleCloudProduct.linkName())
                .proceedToForm(googleCloudProduct.sectionName())
                .fillForm(googleCloudProduct.options());

        String expectedCost = googleCloudProduct.totalEstimatedCost();
        String actualCost = googleCloudCalculatorPage.getEstimatedCost();

        assertThat(expectedCost, is(equalTo(actualCost)));
        
        GoogleCloudSummaryPage summaryPage = googleCloudCalculatorPage.proceedToSummary();
        List<Option> summaryList = summaryPage.getSummaryList();

        for (Option option : googleCloudProduct.options()) {
            summaryList.stream()
                    .filter(o -> o.getName().equalsIgnoreCase(option.getName()))
                    .findFirst()
                    .ifPresent(o -> assertThat(option.getValue().toLowerCase(),
                            is(equalTo(o.getValue().toLowerCase()))));
        }

        actualCost = summaryPage.getTotalEstimatedCost();

        assertThat(expectedCost, is(equalTo(actualCost)));
    }
}
