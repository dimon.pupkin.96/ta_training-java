package task4_tests;

import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.driver.DriverSingleton;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks3_4.util.TestListener;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

@Listeners({TestListener.class})
public class CommonConditions {
    protected WebDriver driver;

    @BeforeMethod()
    public void setUp(){
        driver = DriverSingleton.getDriver();
    }

    @AfterMethod
    public void tearDown(){
        DriverSingleton.closeDriver();
    }
}
