package task1_tests;

import static org.junit.jupiter.api.Assertions.*;

import org.epam.training.student_dmytro_demchuk.fundamental.tasks1_2.classes.pastebin.PasteBinMainPage;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks1_2.classes.pastebin.PasteCreatedPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.stream.Stream;

public class TestNewPaste {
    private WebDriver webDriver;


    @BeforeEach
    public void setUp(){
        webDriver = new ChromeDriver();
    }

    @ParameterizedTest
    @MethodSource("testDataForCreatingNewPaste")
    public void testNewPaste(String code, String pasteName, String expiration, String syntaxHighlighting) {
        PasteCreatedPage pasteCreatedPage = new PasteBinMainPage(webDriver)
                .open().createNewPaste(code, syntaxHighlighting, expiration, pasteName);

        assertTrue(pasteCreatedPage.isPasteCreatedMessageDisplayed());
    }

    @AfterEach
    public void tearDown(){
        webDriver.quit();
    }

    static Stream<Arguments> testDataForCreatingNewPaste() {
        return Stream.of(
                Arguments.of("Hello from WebDriver",
                        "helloweb",
                        "10 Minutes",
                        "None")
        );
    }
}
