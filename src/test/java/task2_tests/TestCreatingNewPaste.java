package task2_tests;

import org.epam.training.student_dmytro_demchuk.fundamental.tasks1_2.classes.pastebin.PasteBinMainPage;
import org.epam.training.student_dmytro_demchuk.fundamental.tasks1_2.classes.pastebin.PasteCreatedPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCreatingNewPaste {
    private WebDriver webDriver;

    @BeforeEach
    public void setUp(){
        webDriver = new ChromeDriver();
    }

    @ParameterizedTest
    @MethodSource("testDataForCreatingNewPaste")
    public void testCreateNewPaste(String code, String pasteName,
                                     String expiration, String syntaxHighlighting
    ) {
        PasteCreatedPage pasteCreatedPage = new PasteBinMainPage(webDriver)
                .open()
                .createNewPaste(code, syntaxHighlighting, expiration, pasteName);

        String actualPasteName = pasteCreatedPage.getCreatedPasteName();
        String actualSyntaxHighlighting = pasteCreatedPage.getCreatedPasteSyntaxHighlighting();

        assertEquals(pasteName, actualPasteName);
        assertEquals(syntaxHighlighting, actualSyntaxHighlighting);
        assertEquals(code, pasteCreatedPage.getCreatedPasteCode());
    }

    @AfterEach public void tearDown(){
        webDriver.quit();
    }

    static Stream<Arguments> testDataForCreatingNewPaste() {
        return Stream.of(
                Arguments.of("git config --global user.name  \"New Sheriff in Town\"\n" +
                        "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                        "git push origin master --force\n",
                        "how to gain dominance among developers",
                        "10 Minutes",
                        "Bash")
        );
    }
}
