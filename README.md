## Task 4. Running Tests

To execute the automated tests, follow these steps:
1. Run the following Maven command:

```bash
mvn -Dbrowser=chrome -Denvironment=test_data_2 "-Dsurefire.suiteXmlFiles=src/test/resources/testng-calculating.xml" clean test
```

This command will:
- Set the browser to Chrome (`-Dbrowser=chrome`) or (`-Dbrowser=firefox`).
- Set the environment to test_data_2 (`-Denvironment=test_data_1`) or (`-Denvironment=test_data_2`).
- Specify the TestNG XML suite file (`-Dsurefire.suiteXmlFiles=src/test/resources/testng-calculating.xml`) or (`-Dsurefire.suiteXmlFiles=src/test/resources/testng-smoke.xml`).
- Clean the project (`clean`).
- Execute the tests (`test`).

4. Wait for the tests to complete. The test results will be displayed in the terminal/command prompt.